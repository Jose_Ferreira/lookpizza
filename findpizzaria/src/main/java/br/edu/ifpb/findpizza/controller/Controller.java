/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizza.controller;

import br.edu.ifpb.findpizza.util.Mensagem;
import br.edu.ifpb.findpizzaria.entity.Localidade;
import br.edu.ifpb.findpizzaria.entity.Usuario;
import br.edu.ifpb.findpizzaria.service.LocalidadeService;
import br.edu.ifpb.findpizzaria.service.UserService;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose
 */
@Named
@RequestScoped
public class Controller {

    private LocalidadeService sl;
    private Localidade localidade;
    private UserService su;
    private String nome;
    @Inject
    private Mensagem mensagem;

    public Controller() {

    }

    @PostConstruct
    public void init() {
        sl = new LocalidadeService();
        localidade = new Localidade();
        su = new UserService();
    }

    public String save() throws ParseException {
        try {
             WKTReader fromText = new WKTReader();

        Geometry geom = fromText.read("POINT(23 43)");
        localidade = Localidade.of(nome, "end 1", geom, 0, "21444", null);

        sl.salve(localidade);
        localidade = new Localidade();
        mensagem.addMessage(nome+": savo com sucesso");
        } catch (Exception e) {
            mensagem.addMessage("erro ao sava "+ e.getMessage());
        }
        return null;
    }

    public Localidade getLocalidade() {
        return localidade;
    }

    public void setLocalidade(Localidade localidade) {
        this.localidade = localidade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public List<Usuario> getUsuarios(){
        List<Usuario> buscarTodos = new ArrayList<>();
        buscarTodos.add(su.buscar("id", 1));
//        for (Usuario b : buscarTodos) {
//            System.err.println("usario"+b);
//        }
        return buscarTodos;
    }    
//    public List<Usuario> getUsuarios(){
//        List<Usuario> buscarTodos = su.buscarTodos();
//        for (Usuario b : buscarTodos) {
//            System.err.println("usario"+b);
//        }
//        return buscarTodos;
//    }    

}

