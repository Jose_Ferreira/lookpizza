/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizzaria.service;

import br.edu.ifpb.findpizzaria.entity.Avaliacao;
import br.edu.ifpb.findpizzaria.repository.AvaliacaoRepository;
import br.edu.ifpb.findpizzaria.repository.AvaliacaoRepositoryImpl;
import java.util.List;

/**
 *
 * @author jose
 */
public class AvaliacaoService {

    private AvaliacaoRepository repository;

    public AvaliacaoService() {
        this.repository = new AvaliacaoRepositoryImpl();
    }

    public void salve(Avaliacao a) {
        repository.salvar(a);
    }
      public void delete(int id_localidade,int id_usuario) {
        repository.deletar(id_localidade,id_usuario);
    }

    public String atualizar(Avaliacao a) {
        this.repository.atualizar(a);
        return null;
    }

    public Avaliacao buscar(String atributo, Object valor) {
        return this.repository.buscar(atributo, valor);

    }

    public List<Avaliacao> buscarTodos() {
        return this.repository.buscarTodos();

    }

    public List<Avaliacao> buscarAvaliacoesPorLocalidade(int idLocalidade) {

        return repository.buscarAvaliacoesPorLocalidade(idLocalidade);
    }

    public List<Avaliacao> buscarAvaliacoesPorUsuario(int idUsuario) {

        return repository.buscarAvaliacoesPorUsuario(idUsuario);
    }
}
