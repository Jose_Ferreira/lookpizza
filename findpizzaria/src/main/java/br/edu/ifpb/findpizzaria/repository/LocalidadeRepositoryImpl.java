/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizzaria.repository;

import br.edu.ifpb.findpizza.conexao.Conexao;
import br.edu.ifpb.findpizza.conexao.DataBaseException;
import br.edu.ifpb.findpizzaria.entity.Avaliacao;
import br.edu.ifpb.findpizzaria.entity.Localidade;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jose
 */
public class LocalidadeRepositoryImpl implements LocalidadeRepository {

    private Conexao conn;

    PreparedStatement stat;
    StringBuffer sql;
    private WKTReader reader;

    public LocalidadeRepositoryImpl() {
        this.sql = new StringBuffer();
        this.conn = new Conexao();
    }

    @Override
    public void salvar(Localidade l) {
        try {

            sql.append("INSERT INTO  localidade (nome,local,endereco,telefone)");
            sql.append("VALUES (?, ST_GeomFromText(?, 26910),?,?)");
            System.err.println("sql " + sql.toString());
            saveBD(l, sql.toString());
        } catch (SQLException | IOException | ClassNotFoundException | URISyntaxException ex) {
            Logger.getLogger(LocalidadeRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void atualizar(Localidade entity) {
        sql.append("UPDATE localidade SET nome =?,local = ST_GeomFromText(?, 26910) ,endereco =?, telefone=? WHERE id =");
        sql.append(entity.getId());
        sql.append("");

        try {
            saveBD(entity, sql.toString());
        } catch (SQLException | IOException | ClassNotFoundException | URISyntaxException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deletar(int chavePrimaria) {
        Connection connection = conn.init();
        try {

            sql.append("DELETE FROM localidade WHERE id =");
            sql.append(chavePrimaria);
            sql.append("");
            stat = connection.prepareStatement(sql.toString());
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.fecharConexao(connection);
            } catch (SQLException ex) {
                Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public Localidade buscar(String atributo, Object valor) {
        sql.append("SELECT id, nome, st_astext(local) as local_text, telefone, endereco,mediaNotas FROM localidade WHERE");
        sql.append(" ");
        sql.append(atributo);
        sql.append("=");
        sql.append(valor);
        sql.append("");
        System.err.println("sql 2 " + sql);
        Localidade resutado = null;
        try {
            resutado = queryBD(sql.toString()).get(0);
        } catch (URISyntaxException | ParseException | IOException | ClassNotFoundException | DataBaseException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resutado;
    }

    @Override
    public List<Localidade> buscarTodos() {
        sql.append("SELECT id, nome, st_astext(local) as local_text, telefone, endereco,mediaNotas FROM localidade");
        List<Localidade> resutado = null;
        try {
            resutado = queryBD(sql.toString());
        } catch (URISyntaxException | ParseException | IOException | ClassNotFoundException | DataBaseException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resutado;
    }

    private void saveBD(Localidade l, String query) throws SQLException, IOException, ClassNotFoundException, URISyntaxException {

        this.conn = new Conexao();
        Connection conection = conn.init();
        try {

            stat = conection.prepareStatement(query);
            stat.setString(1, l.getNome());
            stat.setString(2, l.getLocation().toText());
            stat.setString(3, l.getEndereco());
            stat.setString(4, l.getTelefone());
            stat.executeUpdate();
        } finally {
            conn.fecharConexao(conection);
        }

    }

    private List<Localidade> queryBD(String query) throws URISyntaxException, ParseException, IOException, ClassNotFoundException, DataBaseException, SQLException {

        List<Localidade> result = null;
        Connection connection = conn.init();
        try {
            //  System.err.println("try list");
            System.err.println("sql " + query);
            stat = connection.prepareStatement(query);
            ResultSet rs = stat.executeQuery();
            // System.err.println("passou conn");
            result = montarLocalidade(rs);

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            conn.fecharConexao(connection);
        }

        return result;
    }

    private List<Localidade> montarLocalidade(ResultSet rs) throws ParseException, SQLException {
        List<Localidade> localidades = new ArrayList<>();
        reader = new WKTReader();
        AvaliacaoRepository daoavaliacoes = new AvaliacaoRepositoryImpl();
        int idLocal;

        while (rs.next()) {
            idLocal = rs.getInt("id");
            System.err.println("id  "+idLocal);
            
           Geometry local = reader.read(rs.getString("local_text"));
            Localidade localidade = Localidade.of(idLocal,
                    rs.getString("nome"),
                    rs.getString("endereco"), local,
                    rs.getFloat("mediaNotas"), rs.getString("telefone"), null);
            
           
            List<Avaliacao> buscarAv = daoavaliacoes.buscarAvaliacoesPorLocalidade(7);
            localidade.setAvaliacao(buscarAv);
            localidades.add(localidade);
        }
       
        if (!localidades.isEmpty()) {

            return localidades;
        }
        return Collections.EMPTY_LIST;

    }
}
