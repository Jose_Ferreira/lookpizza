/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizzaria.entity;

/**
 *
 * @author jose
 */
public class Avaliacao {

    private Usuario usuario;
    String comentario;
  private int nota;
    private int idLocalidade;

    public Avaliacao() {
    }

    private Avaliacao(Usuario usuario, String comentario, int nota, int idLocalidade) {
        this.usuario = usuario;
        this.comentario = comentario;
        this.nota = nota;
        this.idLocalidade = idLocalidade;
    }
    /**
     * Método util para criar um objeto do tipo avalição
     * @param usuario
     * @param comentario
     * @param nota
     * @param idLocalidade
     * @return Objeto do tipo Avaliação
     */
     public static Avaliacao of(Usuario usuario, String comentario, int nota, int idLocalidade) {
         validarNota(nota);
        return new Avaliacao(usuario, comentario, nota, idLocalidade);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        validarNota(nota);
        this.nota = nota;
    }

    public int getIdLocalidade() {
        return idLocalidade;
    }

    public void setIdLocalidade(int idLocalidade) {
        this.idLocalidade = idLocalidade;
    }
private static void validarNota(int nota){
    if(nota<0 || nota>10)
         throw new IllegalArgumentException("Digite uma nota entre 1 e 10 ");
}
    @Override
    public String toString() {
        return "Avaliacao{" + "usuario=" + usuario + ", comentario=" + comentario + ", nota=" + nota + ", idLocalidade=" + idLocalidade + '}';
    }
    

}
