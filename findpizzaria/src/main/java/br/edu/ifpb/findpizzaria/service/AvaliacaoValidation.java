/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizzaria.service;

import br.edu.ifpb.findpizzaria.entity.Avaliacao;
import br.edu.ifpb.findpizzaria.entity.Usuario;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.inject.Named;

/**
 *
 * @author jose2
 */
@Named
public class AvaliacaoValidation {

    

    public AvaliacaoValidation() {
    }

    public Map<String, String> execute(Avaliacao avaliacao) {

        Map<String, String> resultado = new HashMap<>();

        if (avaliacao == null) {
            return null;
        }

        if (avaliacao.getComentario() == null || avaliacao.getComentario().trim().isEmpty()
                || avaliacao.getComentario().length() < 10 || avaliacao.getComentario().length() > 100) {
            resultado.put("comentario", "comentario deve conter de 10 a 100 caracteres.");
        }

        if (avaliacao.getIdLocalidade() < 1) {
            resultado.put("local", "Informe uma localidade .");

        }

        if (resultado.isEmpty()) {
            resultado.put("passou", "true");
        } else {
            resultado.put("passou", "false");
        }

        return resultado;
    }

}
