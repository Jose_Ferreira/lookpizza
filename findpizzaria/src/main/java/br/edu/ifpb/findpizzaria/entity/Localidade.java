/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizzaria.entity;

import com.vividsolutions.jts.geom.Geometry;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author jose
 */
public class Localidade {

    private int id;
    private String nome;
    private String endereco;

    private Geometry location;

    private double mediaNotas;
     private String telefone;
   private List<Avaliacao> avaliacao;

    public Localidade() {
    }

    private Localidade(int id, String nome, String endereco, Geometry location, double mediaNotas, String telefone) {
        this.id = id;
        this.nome = nome;
        this.endereco = endereco;
        this.location = location;
        this.mediaNotas = mediaNotas;
        this.telefone = telefone;
       
    }
    private Localidade( String nome, String endereco, Geometry location, double mediaNotas, String telefone) {
       
        this.nome = nome;
        this.endereco = endereco;
        this.location = location;
        this.mediaNotas = mediaNotas;
        this.telefone = telefone;
      
    }
    /**
     * Método util para criar um objeto do tipo localidade
     * @param id
     * @param nome
     * @param endereco
     * @param location
     * @param mediaNotas
     * @param telefone
   
     * @return Objeto do tipo Localidade
     */
   public static Localidade of(int id, String nome, String endereco, Geometry location, double mediaNotas, String telefone, List<Avaliacao> avaliacao) {
       return new Localidade(id, nome, endereco, location, mediaNotas, telefone);
   }
    /**
     * Método util para criar um objeto do tipo localidade
     * @param nome
     * @param endereco
     * @param location
     * @param mediaNotas
     * @param telefone
     * @return Objeto do tipo Localidade
     */
    public static Localidade of( String nome, String endereco, Geometry location, double mediaNotas, String telefone, List<Avaliacao> avaliacao) {
       return new Localidade(nome, endereco, location, mediaNotas, telefone);
   }
       
    

   

    public int getId() {
        return id;
    }

  

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Geometry getLocation() {
        return location;
    }

    public void setLocation(Geometry location) {
        this.location = location;
    }

    public double getMediaNotas() {
        return mediaNotas;
    }

    public void setMediaNotas(double mediaNotas) {
        this.mediaNotas = mediaNotas;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<Avaliacao> getAvaliacao() {
        return Collections.unmodifiableList(avaliacao);
    }

    public void setAvaliacao(List<Avaliacao> avaliacao) {
        this.avaliacao = avaliacao;
    }

    @Override
    public String toString() {
        return "Localidade{" + "id=" + id + ", nome=" + nome + ", endereco=" + endereco + ", location=" + location + ", mediaNotas=" + mediaNotas + ", telefone=" + telefone + ", avaliacao=" + avaliacao + '}';
    }

}
