/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizzaria.service;

import br.edu.ifpb.findpizzaria.entity.Localidade;
import br.edu.ifpb.findpizzaria.repository.LocalidadeRepository;
import br.edu.ifpb.findpizzaria.repository.LocalidadeRepositoryImpl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jose
 */
public class LocalidadeService {
private localidadeValidation validation;
    private LocalidadeRepository repository;

    public LocalidadeService() {
        this.repository = new LocalidadeRepositoryImpl();
        validation = new localidadeValidation();
    }

    public Map<String, String> salve(Localidade l) {
    Map<String, String> execute = validation.execute(l);
    if(execute.get("passou").equalsIgnoreCase("true"))
        repository.salvar(l);
    return execute;
    }

    public Map<String, String> atualizar(Localidade localidade) {
        Map<String, String> execute = validation.execute(localidade);
    if(execute.get("passou").equalsIgnoreCase("true"))
        repository.atualizar(localidade);
    return execute;
    }

    public Localidade buscar(String atributo, Object valor) {
        return this.repository.buscar(atributo, valor);

    }

    public List<Localidade> buscarTodos() {
        return this.repository.buscarTodos();
    }

    public void delete(int chave) {
        repository.deletar(chave);
    }
}
