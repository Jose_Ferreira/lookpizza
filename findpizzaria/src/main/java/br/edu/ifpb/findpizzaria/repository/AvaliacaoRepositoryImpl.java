/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizzaria.repository;

import br.edu.ifpb.findpizza.conexao.Conexao;
import br.edu.ifpb.findpizza.conexao.DataBaseException;
import br.edu.ifpb.findpizzaria.entity.Avaliacao;
import br.edu.ifpb.findpizzaria.entity.Usuario;
import com.vividsolutions.jts.io.ParseException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AvaliacaoRepositoryImpl implements AvaliacaoRepository {

    private Conexao conn;

    PreparedStatement stat;
    StringBuffer sql;

    public AvaliacaoRepositoryImpl() {
        this.sql = new StringBuffer();
        this.conn = new Conexao();
    }

    @Override
    public void salvar(Avaliacao a) {
        try {

            sql.append("INSERT INTO  avaliacao ( comentario, nota, id_usuario, id_localidade)");
            sql.append("VALUES (?,?,?,?)");
            System.err.println("sql " + sql.toString());
            saveBD(a, sql.toString());
        } catch (SQLException | IOException | ClassNotFoundException | URISyntaxException ex) {
            Logger.getLogger(LocalidadeRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void atualizar(Avaliacao entity) {
        sql.append("UPDATE avaliacao SET comentario=?,nota=? WHERE");
        sql.append(" ");
        sql.append("id_usuario=");
        sql.append("?");
        sql.append(" ");
        sql.append("AND id_localidade=");
        sql.append("?");

        try {
            saveBD(entity, sql.toString());
        } catch (SQLException | IOException | ClassNotFoundException | URISyntaxException ex) {
            Logger.getLogger(AvaliacaoRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deletar(int id_localidade, int id_usuario) {
        Connection connection = conn.init();
        try {

            sql.append("DELETE FROM avaliacao WHERE id_localidade =");
            sql.append(id_localidade);
            sql.append(" AND ");
            sql.append("id_usuario= ");
            sql.append(id_usuario);
            sql.append("");
            stat = connection.prepareStatement(sql.toString());
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.fecharConexao(connection);
            } catch (SQLException ex) {
                Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public Avaliacao buscar(String atributo, Object valor) {
        sql.append("SELECT A.id_usuario, A.id_localidade, A.comentario, U.nome nomeUsuario,A.nota");
        sql.append(" ");
        sql.append("FROM avaliacao A, usuario U");
        sql.append(" ");
        sql.append("WHERE U.id = A.id_usuario AND");
        sql.append(" ");
        sql.append(atributo);
        sql.append("=");
        sql.append(valor);
        Avaliacao resutado = null;
        try {
            resutado = queryBD(sql.toString()).get(0);
        } catch (URISyntaxException | ParseException | IOException | ClassNotFoundException | DataBaseException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resutado;
    }

    @Override
    public List<Avaliacao> buscarTodos() {
        sql.append("SELECT A.id_usuario, A.id_localidade, A.comentario, U.nome nomeUsuario, A.nota");
        sql.append(" ");
        sql.append("FROM avaliacao A, usuario U");
        sql.append(" ");
        sql.append("WHERE U.id = A.id_usuario ");
        List<Avaliacao> resutado = null;
        try {
            resutado = queryBD(sql.toString());
        } catch (URISyntaxException | ParseException | IOException | ClassNotFoundException | DataBaseException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resutado;
    }

    private void saveBD(Avaliacao a, String query) throws SQLException, IOException, ClassNotFoundException, URISyntaxException {

        this.conn = new Conexao();
        Connection conection = conn.init();
        try {

            stat = conection.prepareStatement(query);
            stat.setString(1, a.getComentario());
            stat.setInt(2, a.getNota());
            stat.setInt(3, a.getUsuario().getId());
            stat.setInt(4, a.getIdLocalidade());

            stat.executeUpdate();
        } finally {
            conn.fecharConexao(conection);
        }

    }

    private List<Avaliacao> queryBD(String query) throws URISyntaxException, ParseException, IOException, ClassNotFoundException, DataBaseException, SQLException {

        List<Avaliacao> result = new ArrayList<>();
        Connection connection = conn.init();
        try {
            //  System.err.println("try list");
            System.err.println("sql " + query);
            stat = connection.prepareStatement(query);
            ResultSet rs = stat.executeQuery();
            // System.err.println("passou conn");
            result = montarAvaliacao(rs);

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            conn.fecharConexao(connection);
        }
      
        return result;
        
    }

    private List<Avaliacao> montarAvaliacao(ResultSet rs) throws ParseException, SQLException {
        List<Avaliacao> avaliacoes = new ArrayList<>();

        while (rs.next()) {
            Usuario usuario = Usuario.of(rs.getInt("id_usuario"), rs.getString("nomeUsuario"), null, null);
            Avaliacao avaliacao = Avaliacao.of(usuario,
                    rs.getString("comentario"),
                    rs.getInt("nota"), rs.getInt("id_localidade"));
            avaliacoes.add(avaliacao);
            // System.err.println(" user monte ============= " + novo.toString());
        }
        if (!avaliacoes.isEmpty()) {

            return avaliacoes;
        }
        return Collections.EMPTY_LIST;

    }

    
    public List<Avaliacao> buscarAvaliacoesPorLocalidade(int idLocalidade) {
        sql.append("SELECT A.id_usuario, A.id_localidade, A.comentario, U.nome nomeUsuario,A.nota");
        sql.append(" ");
        sql.append("FROM avaliacao A, usuario U");
        sql.append(" ");
        sql.append("WHERE U.id = A.id_usuario AND");
        sql.append(" ");
        sql.append("id_localidade");
        sql.append("=");
        sql.append(idLocalidade);
       
      
        return buscarAvaliacoes(sql.toString());
    }

    @Override
    public List<Avaliacao> buscarAvaliacoesPorUsuario(int idUsuario) {
         sql.append("SELECT A.id_usuario, A.id_localidade, A.comentario, U.nome nomeUsuario,A.nota");
        sql.append(" ");
        sql.append("FROM avaliacao A, usuario U");
        sql.append(" ");
        sql.append("WHERE U.id = A.id_usuario AND");
        sql.append(" ");
        sql.append("id_Usuario");
        sql.append("=");
        sql.append(idUsuario);
        return buscarAvaliacoes(sql.toString());
    }
    
     private List<Avaliacao> buscarAvaliacoes(String query) {
        
        List<Avaliacao> resutado = new ArrayList<>();
        try {
            resutado = queryBD(query);
        } catch (URISyntaxException | ParseException | IOException | ClassNotFoundException | DataBaseException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositotoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resutado;
    }

  
}
