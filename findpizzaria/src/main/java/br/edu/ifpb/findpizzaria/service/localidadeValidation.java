/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizzaria.service;

import br.edu.ifpb.findpizzaria.entity.Localidade;
import br.edu.ifpb.findpizzaria.entity.Usuario;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.inject.Named;

/**
 *
 * @author jose2
 */
@Named
public class localidadeValidation {

    private final Pattern REGEX_TEL_VALIDO = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public localidadeValidation() {
    }

    public Map<String, String> execute(Localidade local) {
        System.err.println("validado" + local.toString());

        Map<String, String> resultado = new HashMap<>();

        if (local == null) {
            return null;
        }

        if (local.getNome() == null || local.getNome().trim().isEmpty()
                || local.getNome().length() < 10 || local.getNome().length() > 60) {
            resultado.put("nome", "Nome deve conter de 10 a 60 caracteres.");
        } else if (!local.getNome().matches("[A-Za-zÀ-ú0-9 ]+")) {
            resultado.put("nome", "Nome não deve conter sibolos especiais (% - $ _ # @, por exemplo).");
        }

        if (local.getEndereco() == null
                || local.getEndereco().trim().isEmpty()) {
            resultado.put("endereco", "informe o endereço  .");

        }
        if (local.getTelefone() == null || local.getTelefone().trim().isEmpty()
                || !REGEX_TEL_VALIDO.matcher(local.getTelefone()).find()) {
            resultado.put("tel", "Informe o telefone corretamente: ex (00)9 9999-9999.");
        }
        if (local.getLocation() == null) {
            resultado.put("local", "Infome o loclizaçao");
        }

        if (resultado.isEmpty()) {
            resultado.put("passou", "true");
        } else {
            resultado.put("passou", "false");
        }

        return resultado;
    }

}
