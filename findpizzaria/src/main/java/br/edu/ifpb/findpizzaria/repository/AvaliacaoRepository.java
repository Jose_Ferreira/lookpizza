/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.findpizzaria.repository;

import br.edu.ifpb.findpizzaria.entity.Avaliacao;
import java.util.List;

/**
 *
 * @author jose
 */
public interface AvaliacaoRepository  {
    void salvar(Avaliacao entity);

    void atualizar(Avaliacao entity);

   
/**
 * 
 * @param atributo  Nome do atributo referente a busca 
 * @param valor valor do atributo referenciado
 * @return Retorna um objeto cuje o atributo for encontrado
 */
    Avaliacao buscar(String atributo, Object valor);

    List<Avaliacao> buscarTodos();

    List<Avaliacao> buscarAvaliacoesPorLocalidade(int idLocalidade);

    public List<Avaliacao> buscarAvaliacoesPorUsuario(int idUsuario);

    void deletar(int id_localidade, int id_usuario);
}
